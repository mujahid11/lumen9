<?php

namespace App\Http\Controllers;

use App\Models\LanguageVariable;
use App\Models\Settings;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function login()
    {
        return view('login');
    }

    public function postLogin(Request $request)
    {
        try {
            $this->validate($request, [
                'email'    => 'required|email|string',
                'password' => 'required|string',
            ]);

            $user = User::where('email', $request->input('email'))->first();
            if (empty($user)) {
                return response()->json(['status' => 'error', 'message' => 'Your credentials does not match']);
            }
            if (Hash::check($request->input('password'), $user->password)) {
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Your credentials does not match']);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => 'error', 'message' => $ex->getMessage()]);
        }

    }
}
